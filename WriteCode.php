
<?php session_start(); ?>

<?php if ( $_SESSION["login"] ==1 ) : ?>

<!DOCTYPE html>

<html lang="en-US">

<head>

	<title>CodeStore - Write Code</title>
	<link rel="stylesheet" type="text/css" href="Styles_WriteCode.css">

</head>

<body>

    <div id="Heading">

    	<p id="display_date"></p>
    	<p id="display_time"></p>

    	<p id="display_username"><?php echo $_SESSION["user"]; ?></p>

		<button id="logout" onclick="closeL()">Log out</button>

	</div>

	<div id="content">

		<p><?php if (isset ($_SESSION["saveCHK"])) { if ($_SESSION["saveCHK"] == 1)	echo $_SESSION["saveCHKmsg1"]."<br>"; else echo $_SESSION["saveCHKmsg0"]."<br>"; }?></p>
			
		<form id = "FormContent" action="SaveCode.php" method="POST">
			
			<div id="codeContent">
			<pre><p>Enter the code here :	 </p><textarea name="codeContent" rows="25" cols="80"></textarea><?php if(isset($_SESSION["codeCHK"])){ if($_SESSION["codeCHK"]==1){echo $_SESSION["codeCHKmsg"];}} ?></p><br></pre>
			</div>
			<div id="codeDetails">
			<pre>Name of file        :	 <input type="text" name="nameOfFile"><?php if(isset($_SESSION["filenameCHK"])){ if($_SESSION["filenameCHK"]==1){echo $_SESSION["filenameCHKmsg"];}} ?></p>
			<br></pre>
			<pre>File type           :	 <select name="typeOfFile"><option value="c">C</option>  											
  													<option value="cpp">C++</option>
  													<option value="java">Java</option>
  													<option value="py">Python</option></select></p><br></pre>		
										  <pre>	<button type="button" onclick="ViewFiles(<?php echo $_SESSION["login"]; ?>)">View saved files</button>      <input type="submit" name="submit" value="Save this code"></button></pre>
			</div>

		</form>

	</div>

	<script type="text/javascript" src="ScriptCodeStore.js"></script>
	<script> 

		setInterval(function(){document.getElementById('display_date').innerHTML = Date().substring( 0, 16);},1000);
 		setInterval(function(){document.getElementById('display_time').innerHTML = Date().substring( 15, 24);},1000);

	</script>

</body>

<?php else : ?>

<script> window.open("index.php","_self"); </script>')

<?php endif; ?>