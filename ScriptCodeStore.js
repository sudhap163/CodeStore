
function nameCheck(str) {

	if (str.length == 0) {

		document.getElementById("name_field").innerHTML = "Required Field";
		return;

	} else {

		var ans = new XMLHttpRequest();
		ans.onreadystatechange = function() {

			if (ans.readyState == 4 && ans.status == 200 ) {

				document.getElementById("name_field").innerHTML = ans.responseText;
			}
		};

		ans.open("GET", "CheckName.php?q=" + str , true);
		ans.send();

	}

};

function phoneCheck(str) {

	if (str.length == 0) {

		document.getElementById("phone_field").innerHTML = "Required Field";
		return;

	} else {

		var ans = new XMLHttpRequest();
		ans.onreadystatechange = function() {

			if (ans.readyState == 4 && ans.status == 200) {

				document.getElementById("phone_field").innerHTML = ans.responseText;

			}
		};

		ans.open("GET", "CheckPhone.php?q=" + str, true);
		ans.send();
	}
};

function emailCheck(str) {

	if (str.length == 0) {

		document.getElementById("email_field").innerHTML = "Required Field";
		return;

	} else {

		var ans = new XMLHttpRequest();
		ans.onreadystatechange = function() {

			if (ans.readyState == 4 && ans.status == 200) {

				document.getElementById("email_field").innerHTML = ans.responseText;

			}
		};

		ans.open("GET", "CheckEmail.php?q=" + str, true);
		ans.send();
	}
};

function usernameCheck(str) {

	if (str.length == 0) {

		document.getElementById("username_field").innerHTML = "Required Field";
		return;

	} else {

		var ans = new XMLHttpRequest();
		ans.onreadystatechange = function() {

			if (ans.readyState == 4 && ans.status == 200) {

				document.getElementById("username_field").innerHTML = ans.responseText;

			}
		};

		ans.open("GET", "CheckUsername.php?q=" + str, true);
		ans.send();
	}
};

function pswdCheck(try2, try1) {

	if (try1.length == 0) {

		document.getElementById("pswd_field").innerHTML = "Fill above input field first";
		return;

	} else {

		if (!(try1 === try2)) {

			document.getElementById("pswd_field").innerHTML = "Passwords donot match";

		} else {

			document.getElementById("pswd_field").innerHTML = "Password is perfect!!";

		}

	}

};

function closeL() {

	
	var ans = new XMLHttpRequest();
	ans.onreadystatechange = function() {

		if (ans.readyState == 4 && ans.status == 200 ) {

			window.open(ans.responseText, "_self" );


		}
	};

	str = "";

	ans.open("GET", "UserLogout.php?q=" + str, true);
	ans.send();

};

function ViewFiles( n ) {

	if (n==1) {

		window.open("ViewCode.php");
	}

	else {

		window.open("index.php", "_self");
	}

};

function ReadFiles( fileN ) {

	

	var ans = new XMLHttpRequest();
	var d = document.getElementById("nameOfFile").options[fileN].text;//To fetch the value text of options under the ID nameOfFile with fileN as the index of selected option
	ans.onreadystatechange = function() {

		if (ans.readyState == 4 && ans.status == 200 ) {
			document.getElementById("codeDisplay").innerHTML = ans.responseText;
		}
	};

	ans.open("GET", "FetchFile.php?q=" + d , true);
	ans.send();

};