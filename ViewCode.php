
<?php session_start(); ?>

<?php if($_SESSION["login"]==1): ?>

<!DOCTYPE html>

<html lang="en-US">

<head>

	<title>CodeStore - View Code</title>
	<link rel="stylesheet" type="text/css" href="Styles_ViewCode.css">

</head>

<body>

    <div id="Heading">

    	<p id="display_date"></p>
    	<p id="display_time"></p>

    	<p id="display_username"><?php echo $_SESSION["user"]; ?></p>

    	<button id="logout" onclick="closeL()">Log out</button>

    </div>

	<?php

		$conn = mysqli_connect('localhost', 'root', 'Sonu&Sona') or die ('Could not connect to server');
		mysqli_select_db($conn, 'CodeStore') or die ('Could not select database');

		$query = "SELECT * FROM FileDetails WHERE username='".$_SESSION["user"]."'";
		$result = mysqli_query($conn,$query);

		$result_array = array();
		while($row = mysqli_fetch_assoc($result)) {

			$result_array[] = $row['filename'];

		}

	?>

	<div id="Content">

	<pre><h3 id="head">Choose a file :	 <select id="nameOfFile" onchange="ReadFiles(this.selectedIndex)"><option value="--1">Select Here</option>
	<?php 
	for ($arr=0;$arr<count($result_array);$arr++) 
		{ echo "<option value=\"".$result_array[$arr]."\">".$result_array[$arr]."</option>";
		  echo "\n"; }
	?></select></h3></pre>

	</div>
	
	<hr>

	
	<!--<p id="codeDisplay"></p>-->
	<textarea id="codeDisplay" cols="100" rows="23" readonly></textarea>

	<script type="text/javascript" src="ScriptCodeStore.js"></script>
	<script> 
		
		setInterval(function(){document.getElementById('display_date').innerHTML = Date().substring( 0, 16);},1000);
 		setInterval(function(){document.getElementById('display_time').innerHTML = Date().substring( 15, 24);},1000);

	</script>


</body>

</html>

<?php else : ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<script type="text/javascript"> window.open("index.php", _self);</script>
</body>
</html>

<?php endif;?>